resource "aws_ecr_repository" "ci" {
  name                 = "ci"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_iam_access_key" "ecr_ci_push" {
  user = aws_iam_user.ecr_ci_push.name
}

resource "aws_iam_user" "ecr_ci_push" {
  name = "GitlabRunnerECS-ECRPush"
}

resource "aws_iam_user_policy" "ecr_ci_push" {
  name = "GitlabRunnerECS-ECRPushPolicy"
  user = aws_iam_user.ecr_ci_push.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ecr:CompleteLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:InitiateLayerUpload",
        "ecr:BatchCheckLayerAvailability",
        "ecr:PutImage"
      ],
      "Effect": "Allow",
      "Resource": "${aws_ecr_repository.ci.arn}"
    },
    {
      "Effect": "Allow",
      "Action": "ecr:GetAuthorizationToken",
      "Resource": "*"
    }
  ]
}
EOF
}

output "ecr_repository_address" {
  description = "Full ECR Repository address"
  value       = aws_ecr_repository.ci.repository_url
}