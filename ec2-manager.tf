locals {
  user_data = <<-EOT
  #!/bin/bash
  FARGATE_CONFIG=/etc/gitlab-runner/config.toml
  yum install -y openssh git
  # apt-get install -y openssh-server && systemctl enable --now ssh
  systemctl enable --now sshd
  curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
  rpm -i gitlab-runner_amd64.rpm
  # sudo mkdir -p /opt/gitlab-runner/{metadata,builds,cache}
  # curl -s "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
  # apt-get install -y gitlab-runner
  tee -a /etc/gitlab-runner/fargate.toml <<FARGATE
  LogLevel = "info"
  LogFormat = "text"

  [Fargate]
    Cluster = "gitlab_runners"
    Region = "us-east-1"
    Subnet = "subnet-2c37df75"
    SecurityGroup = "${aws_security_group.gitlab_runner_fargate.id}"
    TaskDefinition = "ecs_gitlab_runner:${aws_ecs_task_definition.gitlab_runners.revision}"
    EnablePublicIP = true

  [TaskMetadata]
    Directory = "/opt/gitlab-runner/metadata"

  [SSH]
    Username = "root"
    Port = 22
  FARGATE
  mkdir -p /opt/gitlab-runner/metadata
  curl -Lo /opt/gitlab-runner/fargate "https://gitlab-runner-custom-fargate-downloads.s3.amazonaws.com/latest/fargate-linux-amd64"
  chmod +x /opt/gitlab-runner/fargate
  gitlab-runner register --url https://gitlab.com/ \
                          --registration-token ${var.GITLAB_REGISTRATION_TOKEN} \
                          --name fargate-test-runner \
                          --run-untagged \
                          --executor custom \
                          -n
  sed -i s/'concurrent = 1'/'concurrent = 10'/g $FARGATE_CONFIG
  sed -i '/^\[\[runners\]\]/a \ \ limit = 10' $FARGATE_CONFIG
  sed -i '/^\[\[runners\]\]/a \ \ builds_dir = "/opt/gitlab-runner/builds"' $FARGATE_CONFIG
  sed -i '/^\[\[runners\]\]/a \ \ cache_dir = "/opt/gitlab-runner/cache"' $FARGATE_CONFIG
  sed -i '/run_exec.*/d' config.toml.orig $FARGATE_CONFIG
  tee -a $FARGATE_CONFIG << CUSTOM_RUNNER_CONFIG
      volumes = ["/cache", "/path/to-ca-cert-dir/ca.crt:/etc/gitlab-runner/certs/ca.crt:ro"]
      config_exec = "/opt/gitlab-runner/fargate"
      config_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "config"]
      prepare_exec = "/opt/gitlab-runner/fargate"
      prepare_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "prepare"]
      run_exec = "/opt/gitlab-runner/fargate"
      run_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "run"]
      cleanup_exec = "/opt/gitlab-runner/fargate"
      cleanup_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "cleanup"]
  CUSTOM_RUNNER_CONFIG
  kill -SIGHUP $(pgrep -f gitlab | head -1) # reload gitlab-runner configuration
  EOT
}

resource "aws_instance" "gitlab_runner_manager" {
  ami                         = "ami-0b0dcb5067f052a63"
  instance_type               = "t3a.micro"
  key_name                    = "nt-e6430"
  vpc_security_group_ids      = ["sg-92460cf7"]
  subnet_id                   = "subnet-2c37df75"
  iam_instance_profile        = aws_iam_instance_profile.manager_ec2_profile.name
  user_data_base64            = base64encode(local.user_data)
  user_data_replace_on_change = true # set true if changing user data. bug where it seems to always want to replace it

  tags = {
    Environment = "dev"
    Name        = "gitlab_runner_manager"
    Terraform   = "true"
  }
}

resource "aws_eip" "gitlab_runner_manager_eip" {
  instance = aws_instance.gitlab_runner_manager.id
  vpc      = true
}

output "instance_eip" {
  description = "Elastic public IP attached to instance"
  value       = aws_eip.gitlab_runner_manager_eip.public_ip
}

output "ssh_to_instance_command" {
  description = "Preformed SSH command for connecting to instance"
  value       = "ssh -i ~/Dropbox/machines/ssh/nt-e6430/id_rsa ec2-user@${aws_eip.gitlab_runner_manager_eip.public_ip}"
}