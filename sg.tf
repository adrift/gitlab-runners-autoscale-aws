resource "aws_security_group" "gitlab_runner_fargate" {
  name        = "gitlab_runner_fargate"
  description = "SG for ECS gitlab runners. Allow SSH from runner manager, all outbound traffic"
  vpc_id      = var.vpc_id

  tags = {
    Name = "gitlab_runner_fargate"
  }
}

resource "aws_security_group_rule" "allow_outbound" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]
  # ipv6_cidr_blocks  = [aws_vpc.example.ipv6_cidr_block]
  security_group_id = aws_security_group.gitlab_runner_fargate.id
}

resource "aws_security_group_rule" "allow_ssh_from_runner_manager_sg" {
  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  # cidr_blocks       = [aws_vpc.example.cidr_block]
  # ipv6_cidr_blocks  = [aws_vpc.example.ipv6_cidr_block]
  security_group_id        = aws_security_group.gitlab_runner_fargate.id
  source_security_group_id = "sg-92460cf7"
}

resource "aws_security_group_rule" "allow_ssh_from_runner_manager_eip" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["${aws_eip.gitlab_runner_manager_eip.public_ip}/32"]
  # ipv6_cidr_blocks  = [aws_vpc.example.ipv6_cidr_block]
  security_group_id = aws_security_group.gitlab_runner_fargate.id
  # source_security_group_id = "sg-92460cf7"
}