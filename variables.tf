variable "vpc_id" {
  default = "vpc-3d2d8358"
}

variable "GITLAB_REGISTRATION_TOKEN" {
  description = "Expected from CI/CD variables. Used to register manager and autoscaling runners"
  type        = string
}

# https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry
variable "GITLAB_DEPLOY_TOKEN_USER" {
  description = "Expected from CI/CD variables. Deploy token to store in Secrets Manager. Will be used read-only to allow ECS to get runtime Docker images"
  type        = string
}

variable "GITLAB_DEPLOY_TOKEN_PASS" {
  description = "Expected from CI/CD variables. Deploy token to store in Secrets Manager. Will be used read-only to allow ECS to get runtime Docker images"
  type        = string
}