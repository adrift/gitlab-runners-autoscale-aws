locals {
  GITLAB_DEPLOY_TOKEN = {
    username = var.GITLAB_DEPLOY_TOKEN_USER,
    password = var.GITLAB_DEPLOY_TOKEN_PASS
  }
}

resource "aws_secretsmanager_secret" "gitlab_deploy_token" {
  name = "ecs_gitlab_runners/gitlab_deploy_token_ci_image_5"
}

resource "aws_secretsmanager_secret_version" "gitlab_deploy_token" {
  secret_id     = aws_secretsmanager_secret.gitlab_deploy_token.id
  secret_string = jsonencode(local.GITLAB_DEPLOY_TOKEN)
}

output "gitlab_deploy_token_secret_arn" {
  description = "ARN for gitlab deploy token secret"
  value       = aws_secretsmanager_secret_version.gitlab_deploy_token.arn
}