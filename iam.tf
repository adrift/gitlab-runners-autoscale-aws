data "aws_caller_identity" "current" {}

# Create ecs execution role
resource "aws_iam_role" "ecs_execution_role" {
  name               = "GitlabRunnerECSExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_ecs.json
}

# Create ecs service role
resource "aws_iam_role" "ecs_runtime_role" {
  name               = "GitlabRunnerECSRuntimeRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_ecs.json
}

resource "aws_iam_instance_profile" "manager_ec2_profile" {
  name = "GitlabRunnerManagerInstanceProfile"
  role = aws_iam_role.ec2_role.name
}

# Create ecs instance profile
resource "aws_iam_instance_profile" "runner_instance_profile" {
  name = "GitlabRunnerInstanceProfile"
  role = aws_iam_role.ecs_runtime_role.name
}

# Attach policy to ecs instance role
resource "aws_iam_role_policy_attachment" "ec2_container_service_for_ec2_role" {
  role       = aws_iam_role.ecs_runtime_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecs_runtime_session_manager" {
  role       = aws_iam_role.ecs_runtime_role.name
  policy_arn = aws_iam_policy.ecs_task_allow_session_manager.arn
}

# Attach policy to ecs instance role
resource "aws_iam_role_policy_attachment" "allow_ecs_service_access_gitlab_secrets" {
  role       = aws_iam_role.ecs_runtime_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

# Attach policy to ecs instance role
resource "aws_iam_role_policy_attachment" "EC2_AmazonECS_FullAccess" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role" "ec2_role" {
  name = "EC2ManagerRole"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# Create policy document for ecs service role
data "aws_iam_policy_document" "assume_role_ecs" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com", "ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "ecs_task_allow_session_manager" {
  name = "GitlabRunnerTaskAllowSessionManager"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "AllowRunTask",
        "Effect" : "Allow",
        "Action" : [
          "ssmmessages:CreateControlChannel",
          "ssmmessages:CreateDataChannel",
          "ssmmessages:OpenControlChannel",
          "ssmmessages:OpenDataChannel"
        ],
        "Resource" : [
          "*"
        ]
      }
    ]
  })
}
resource "aws_iam_policy" "ec2_manager_allow_manage_tasks" {
  name = "GitlabRunnerManagerAllowECSTasks"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "AllowRunTask",
        "Effect" : "Allow",
        "Action" : [
          "ecs:RunTask",
          "ecs:ListTasks",
          "ecs:StartTask",
          "ecs:StopTask",
          "ecs:ListContainerInstances",
          "ecs:DescribeTasks",
          "iam:PassRole"
        ],
        "Resource" : [
          "arn:aws:ecs:us-east-1:${data.aws_caller_identity.current.account_id}:task/gitlab_runners/*",
          "arn:aws:ecs:us-east-1:${data.aws_caller_identity.current.account_id}:task-definition/ecs_gitlab_runner:*",
          "arn:aws:ecs:us-east-1:${data.aws_caller_identity.current.account_id}:cluster/gitlab_runners",
          "${aws_ecs_task_definition.gitlab_runners.arn}/*:*",
          "arn:aws:ecs:*:${data.aws_caller_identity.current.account_id}:container-instance/*/*",
          "${aws_iam_role.ecs_execution_role.arn}"
        ]
      },
      {
        "Sid" : "AllowListTasks",
        "Effect" : "Allow",
        "Action" : [
          "ecs:ListTaskDefinitions",
          "ecs:DescribeTaskDefinition"
        ],
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ec2_policy_role" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.ec2_manager_allow_manage_tasks.arn
}

# Attach policy to ecs instance role
resource "aws_iam_role_policy_attachment" "ecs_execution_secretmanager" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = aws_iam_policy.access_gitlab_deploy_token.arn
}

resource "aws_iam_role_policy_attachment" "ecs_execution_logs" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = aws_iam_policy.put_cloudwatch_logs.arn
}

resource "aws_iam_role_policy_attachment" "ecs_execution_ecr_token" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = aws_iam_policy.get_ecr_token.arn
}

resource "aws_iam_role_policy_attachment" "ecs_execution_ecr_read" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "ecs_runtime_secretmanager" {
  role       = aws_iam_role.ecs_runtime_role.name
  policy_arn = aws_iam_policy.access_gitlab_deploy_token.arn
}

resource "aws_iam_policy" "access_gitlab_deploy_token" {
  name        = "ReadSecretGitlabDeployToken"
  path        = "/"
  description = "Allows Get/Reveal on GitLab deploy token secret stored in Secret Manager"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "kms:DecryptSecret",
          "ssm:GetParameters",
          "secretsmanager:GetSecretValue"
        ]
        Effect   = "Allow"
        Resource = "${aws_secretsmanager_secret_version.gitlab_deploy_token.arn}"
      },
    ]
  })
}
# Resource = "${aws_secretsmanager_secret_version.gitlab_deploy_token.arn}"

resource "aws_iam_policy" "put_cloudwatch_logs" {
  name        = "GitlabRunnerECSPutCloudwatchLogs"
  path        = "/"
  description = "Allows Gitlab Runners in ECS to publish logs to Cloudwatch Logs"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ]
        Effect = "Allow"
        Resource = [
          "${aws_cloudwatch_log_group.logs.arn}",
          "arn:aws:logs:us-east-1:133163756620:log-group:/gitlab-runners/generic:log-stream:ecs/ci-coordinator/*"
        ]
      },
    ]
  })
}

resource "aws_iam_policy" "get_ecr_token" {
  name        = "GitlabRunnerECSGetECRImage"
  path        = "/"
  description = "Allows Gitlab Runners in ECS to fetch ECR credentials"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ecr:GetAuthorizationToken"
        ]
        Effect = "Allow"
        Resource = [
          "*"
        ]
      },
    ]
  })
}

# Attach policy to GitLab user
# resource "aws_iam_user_policy_attachment" "gitlab_s3_full_access" {
#   user       = "${aws_iam_user.gitlab.name}"
#   policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
# }

# # Attach policy to GitLab Runner user
# resource "aws_iam_user_policy_attachment" "gitlab_runner_ec2_full_access" {
#   user       = "${aws_iam_user.gitlab_runner.name}"
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
# }

# resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
#   role       = aws_iam_role.ecs_execution_role.name
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
# }

# resource "aws_iam_role_policy_attachment" "task_s3" {
#   role       = aws_iam_role.gitlab_runner_ecs_task_role.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
# }

output "execution_role_arn" {
  description = "gitlab runner exec role arn"
  value       = aws_iam_role.ecs_execution_role.arn
}