resource "aws_cloudwatch_log_group" "logs" {
  name              = "/gitlab-runners/generic"
  retention_in_days = 90
}