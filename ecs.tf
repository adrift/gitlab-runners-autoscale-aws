resource "aws_ecs_cluster" "gitlab_runners" {
  name = "gitlab_runners"
}

resource "aws_ecs_cluster_capacity_providers" "basic" {
  cluster_name = aws_ecs_cluster.gitlab_runners.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

resource "aws_ecs_task_definition" "gitlab_runners" {
  family                   = "ecs_gitlab_runner"
  cpu                      = 256 # 1024 = 1 vCore
  memory                   = 512 # MiB
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  # "registry.gitlab.com/tmaczukin-test-projects/fargate-driver-debian:latest"
  # "registry.gitlab.com/ecmc-group/systems-engineering/docker/ci:SET-1555-ecs-compatible" ubuntu-based
  #  al2-based
  # "133163756620.dkr.ecr.us-east-1.amazonaws.com/ci:SET-1555-al2-ecs-compat" al2-based, ecr mirrored
  container_definitions = jsonencode([
    {
      name      = "ci-coordinator"
      image     = "133163756620.dkr.ecr.us-east-1.amazonaws.com/ci:SET-1555-al2-ecs-compat"
      essential = true

      # repositoryCredentials = {
      #   credentialsParameter = "${aws_secretsmanager_secret_version.gitlab_deploy_token.arn}"
      # }

      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = "/gitlab-runners/generic"
          awslogs-region        = "us-east-1"
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
  execution_role_arn = aws_iam_role.ecs_execution_role.arn
  task_role_arn      = aws_iam_role.ecs_runtime_role.arn

  #   # volume {
  #   #   name      = "service-storage"
  #   #   host_path = "/ecs/service-storage"
  #   # }

  #   # placement_constraints {
  #   #   type       = "memberOf"
  #   #   expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #   # }
}